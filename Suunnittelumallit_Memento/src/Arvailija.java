import java.util.Random;

public class Arvailija extends Thread{
	
	private Object object;
	private boolean oikein = false;
	private int arvottu;
	private Arvuuttaja arvuuttaja;
	private int saie;
	public static int seuraavasaie=1;
	
	public Arvailija(Arvuuttaja arvuuttaja) {
		this.arvuuttaja = arvuuttaja;
		this.saie = seuraavasaie++;
	}
	
	public void liityPeliin() {
		this.object = this.arvuuttaja.liityPeliin();
	}
	
	public void arvaaNumero() {
		Random rand = new Random();
		this.arvottu = rand.nextInt(10)+1;
	}
	
	public void tarkastaArvaus() {
		oikein = this.arvuuttaja.tarkastaArvaus(object, this.arvottu);
	}
	
	public void run() {
		while(!oikein) {
			arvaaNumero();
			System.out.println("Säie numero "+ saie + " arvaa: " + this.arvottu);
			tarkastaArvaus();
			
		}
		System.out.println("Säie numero " + saie + " pääsi pois pelistä");
	}

}
