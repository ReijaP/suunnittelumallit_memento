
public class Main {

	public static void main(String[] args) {
		
		Arvailija[] arvailija = new Arvailija[3];
		Arvuuttaja arvuuttaja = new Arvuuttaja();
		
		for(int i = 0; i<arvailija.length;i++) {
			arvailija[i] = new Arvailija(arvuuttaja);
			arvailija[i].liityPeliin();
			arvailija[i].start();
		}

	}

}
