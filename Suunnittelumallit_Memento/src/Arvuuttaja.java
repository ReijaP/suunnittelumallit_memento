import java.util.Random;

public class Arvuuttaja {
	
	public Arvuuttaja() {
	}
	
	public int arvoLuku() {
		Random rand = new Random();
		int arvottu = rand.nextInt(10)+1;
		System.out.println("Arvottu luku on " + arvottu);
		return arvottu;
	}
	
	public Object liityPeliin() {
		int arvottu = arvoLuku();
		return new Memento(arvottu);
	 }
	
	public boolean tarkastaArvaus(Object object, int arvaus) {
		Memento memento = (Memento)object;
		if (arvaus == memento.numero) {
			System.out.println("\nArvaus " + arvaus + " on oikein!\n");
			return true;
		}else
			System.out.println("Arvaus " + arvaus + " on väärin. Arvaa uudelleen!");
		return false;
	}
	
	private class Memento{
		
		private int numero;

		public Memento(int luku) {
			this.numero = new Integer(luku);
		}
		
	}

}
